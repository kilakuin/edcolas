package edlislas;

/**
 *
 * @author chris
 */
public class Nodo {
    public String dato;    
    public Nodo siguiente;  
    public int datoE;
    public Nodo siguienteE;
    
    public Nodo(String d){
        this.dato = d;
    }
        public Nodo(int E){
        this.datoE = E;
    }

    public Nodo(String d, Nodo n){
        dato = d;
        siguiente = n;
    }
        public Nodo(int E, Nodo n){
        datoE = E;
        siguienteE = n;
    }
}