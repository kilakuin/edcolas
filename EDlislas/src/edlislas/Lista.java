package edlislas;

/**
 *
 * @author chris
 */
public class Lista {
    protected Nodo inicio, fin; 
    public Lista(){
        inicio = null;
        fin    = null;
    }
    
    public void agregarAlInicio(String elemento){
        inicio = new Nodo(elemento, inicio);
        if (fin == null){
            fin = inicio;
        }
    }
        public void agregarAlInicioE(int elementoE){
        inicio = new Nodo(elementoE, inicio);
        if (fin == null){
            fin = inicio;
        }
    }
    
    
    public void mostrarListaEnlazada(){
        Nodo recorrer = inicio;
        System.out.println("");
        while (recorrer != null){
            System.out.print("["+ recorrer.dato+"] --");
            recorrer = recorrer.siguiente;
        }
        System.out.println("");
    }
    
    public String borrarDelInicio(){
        String elemento = inicio.dato;
        if (inicio == fin){
            inicio = null;
            fin    = null;
        } else {
            inicio = inicio.siguiente;
        }
        return elemento;
    }
    public void mostrarListaEnlazadaE(){
        Nodo recorrer = inicio;
        System.out.println("");
        while (recorrer != null){
            System.out.print("["+ recorrer.datoE+"] --");
            recorrer = recorrer.siguienteE;
        }
        System.out.println("");
    }
    
    public int borrarDelInicioE(){
        int elementoE = inicio.datoE;
        if (inicio == fin){
            inicio = null;
            fin    = null;
        } else {
            inicio = inicio.siguienteE;
        }
        return elementoE;
    }
    
}