package edlistas2;
import java.util.Scanner;
public class EDlistas2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
 Lista2 lista=new Lista2();
        int Emenu=0,x;
        do{
           try{
                Scanner sc= new Scanner(System.in);
                System.out.println("-----------------------------\n" +
                                   "1.- Añadir valor al principio\n" +
                                   "2.- Borrar valor al principio\n" +
                                   "3.- Añadir valor al Final\n" +
                                   "4.- Borrar valor al Final\n" +
                                   "5.- Mostrar Lista\n" +
                                   "9.- Salir\n"+
                                   "-----------------------------");
                
                System.out.println("Elige una opcion");
                Emenu = sc.nextInt();
              
                switch (Emenu){
                    
                case 1:
                    try{
                       System.out.println("Añadir valor al principio");
                        x=sc.nextInt();
                        lista.agregarAlInicioE(x);
                    }catch(NumberFormatException n){
                         System.out.println("La Lista esta vacia");
                    }
                    break;
                
                case 2:
                    try{
                    x=lista.borrarDelInicioE();
                    System.out.println("Borrar valor al principio");
                    }catch(NumberFormatException n){
                         System.out.println("La Lista esta vacia");
                    }
                    break;
                case 3:
                    try{
                       System.out.println("Añadir valor al Final");
                        x=sc.nextInt();
                        lista.agregarAlFinalE(x);
                    }catch(NumberFormatException n){
                         System.out.println("La Lista esta vacia");
                    }
                    break;
                case 4:
                    try{
                    x=lista.borrarDelFinalE();
                    System.out.println("Borrar valor al Final");
                    }catch(NumberFormatException n){
                         System.out.println("La Lista esta vacia");
                    }
                    break;
                case 5:
                     System.out.println("================");
                    lista.mostrarListaEnlazadaE();
                    break;
                    
                case 9:
                    Emenu = 9;
                    break;
                    
                default:
                  System.out.println("Lista terminada");
                  break;  
                }   
           } catch(Exception e){
               System.out.println("Entrada invalida");
           }
        }while(Emenu!=9);
    }
    
}