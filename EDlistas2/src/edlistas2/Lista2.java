package edlistas2;
/**
 *
 * @author chris
 */
public class Lista2 {
    protected Nodo2 inicio, fin; 
    
    public Lista2(){
        inicio = null;
        fin    = null;
    }
        public boolean ListaVacia(){
        if(inicio==null){
            return true;
        }else{
            return false;
        }
    }
        public void agregarAlInicioE(int elementoE){
        inicio = new Nodo2(elementoE, inicio);
        if (fin == null){
            fin = inicio;
        }
    }
        public void agregarAlFinalE(int elementoE){
        if(!ListaVacia()){
            fin.siguienteE=new Nodo2(elementoE);
            fin=fin.siguienteE;
        }else{
            inicio=fin=new Nodo2(elementoE);
        }
    }
   
    public void mostrarListaEnlazadaE(){
        Nodo2 recorrer=inicio;
        while(recorrer!=null){
            System.out.print("["+recorrer.datoE+"]");
            recorrer=recorrer.siguienteE;
        }
        System.out.println("");
    }
    
    public int borrarDelInicioE(){
        int elementoE = inicio.datoE;
        if (inicio == fin){
            inicio = null;
            fin    = null;
        } else {
            inicio = inicio.siguienteE;
        }
        return elementoE;
    }
    public int borrarDelFinalE(){
        int elemento=fin.datoE;
        if(inicio==fin){
            inicio=fin=null;
        }else{
            Nodo2 temporal=inicio;
            while(temporal.siguienteE!=fin){
                temporal=temporal.siguienteE;
            }
            fin=temporal;
            fin.siguienteE=null;
        }
        return elemento;
    }
}